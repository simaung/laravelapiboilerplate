<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use App\Models\GlobalSetting;
use Illuminate\Http\Request;

class ToolsController extends BaseController
{
    public function appVersion()
    {
        $setting = GlobalSetting::where(['group' => 'app', 'name' => 'version'])->first();
        return $this->sendResponse($setting);
    }
}
