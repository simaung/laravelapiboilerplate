<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;
use Image;

class BaseController extends Controller
{
    public function sendResponse($result, $message = 'Retrieve sucessfully', $code = 200)
    {
        $response = [
            'code'    => $code,
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        return response()->json($response, 200);
    }

    public function sendError($code_custom, $error, $errorMessages = [], $code = 200)
    {
        $response = [
            'code'    => $code_custom,
            'success' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }

    public function failsValidate($validator)
    {
        $msg = '';
        foreach ($validator->toArray() as $key => $row) {
            $msg .= $row[0] . '\n';
        }

        $response = [
            'code'    => 400,
            'success' => false,
            'message' => $msg,
        ];
        return response()->json($response, 200);
    }

    protected function setCookie($key, $value)
    {
        return Cookie::make(
            $key, // Name
            $value, // Value
            config('jwt.ttl'), // time to expire
            '/', // Path
            config('session.domain'), // Domain
            false, // Secure
            true, // httpOnly
            false, // Raw
            'strict' //same-site   <-----
        );
    }
}
