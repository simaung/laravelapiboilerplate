<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\CategoriesResource;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends BaseController
{
    public function __construct(Request $request)
    {
        $this->perPage = (@$request->perPage) ? $request->perPage : 10;
    }


    public function index(Request $request)
    {
        $categories = Category::active();

        if ($request->slug) {
            $categories->where('slug', $request->slug);
        }

        $categories = new CategoriesResource($categories->paginate($this->perPage));

        return $this->sendResponse($categories);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->post(), [
            'name' => 'required',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failsValidate($validator->errors());
        }

        $user = Category::create([
            'name' => $request->name,
            'image' => $request->image,
        ]);

        return $this->sendResponse($user, 'Saved successfully');
    }
}
