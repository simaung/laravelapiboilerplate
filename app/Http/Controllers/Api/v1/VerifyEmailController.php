<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\BaseController;
use Illuminate\Auth\Events\Verified;
use App\Models\User;
use Illuminate\Http\Request;

class VerifyEmailController extends BaseController
{
    public function verify(Request $request)
    {
        $user = User::find($request->route('id'));

        if ($user->hasVerifiedEmail()) {
            return redirect((env('FRONT_URL') . '/email/verify/already-success'));
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }
        return redirect((env('FRONT_URL') . '/email/verify/success'));
    }

    public function resend()
    {
        if (auth()->user()->hasVerifiedEmail()) {
            return $this->sendError(400, 'Email already verified.');
        }

        auth()->user()->sendEmailVerificationNotification();

        return $this->sendResponse([], 'Email verification has been send to your email.');
    }

    public function notice()
    {
        if (auth()->user()->hasVerifiedEmail()) {
            return $this->sendResponse([], 'Email already verified.');
        }

        return $this->sendError(400, 'Email has not verification, please verification first.');
    }
}
