<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends BaseController
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return $this->failsValidate($validator->errors());
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        event(new Registered($user));

        return $this->sendResponse($user, 'Register has been succesfully, Email verification has been send to your email.');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:50',
        ]);

        if ($validator->fails()) {
            return $this->failsValidate($validator->errors());
        }

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->sendError(400, 'Invalid credentials.');
            }
        } catch (JWTException $e) {
            return $credentials;
            return $this->sendError(500, 'Could not create token.');
        }

        $user = auth()->user();
        $user->id = enkrip($user->id);

        $data = $user;
        $data['token'] = $token;
        return $this->sendResponse($data, 'Login Succesfully')->withCookie($this->setCookie('token', $token));
    }

    public function logout()
    {
        try {
            JWTAuth::invalidate();
            $cookie = Cookie::forget('token');
            return $this->sendResponse([], 'Logout Succesfully.')->withCookie($cookie);
        } catch (JWTException $e) {
            dd($e);
            return $this->sendError(500, 'Logout failed.');
        }
    }

    public function getUser()
    {
        $user = auth()->user();
        $user->id = enkrip($user->id);
        return $this->sendResponse($user, 'Retrieve succesfully.');
    }
}
