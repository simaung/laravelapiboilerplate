<?php

namespace App\Models;

use App\Http\Traits\EncryptId;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GlobalSetting extends Model
{
    use HasFactory, EncryptId;

    public function getIdAttribute($value)
    {
        return $this->encryptId($value);
    }
}
