<?php

namespace App\Models;

use App\Http\Traits\EncryptId;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use HasFactory, Sluggable, EncryptId;

    protected $fillable = ['name', 'image'];
    protected $hidden = ['status', 'sort_order', 'parent_id', 'created_at', 'updated_at'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getIdAttribute($value)
    {
        return $this->encryptId($value);
    }

    public function scopeActive()
    {
        return $this->where('status', 'active');
    }
}
