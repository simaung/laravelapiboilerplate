<?php

use App\Http\Controllers\Api\ToolsController;
use App\Http\Controllers\Api\v1\CategoryController;
use App\Http\Controllers\Api\v1\UserController;
use App\Http\Controllers\Api\v1\VerifyEmailController;
use Illuminate\Support\Facades\Route;

//Tool Routes
Route::get('appVersion', [ToolsController::class, 'appVersion']);


// V1 Routes
Route::prefix('v1')->group(function () {
    //Auth Routes
    Route::prefix('auth')->group(function () {
        Route::post('/register', [UserController::class, 'register']);
        Route::post('/login', [UserController::class, 'login']);
        Route::group(
            ['middleware' => ['jwt.verify']],
            function () {
                Route::post('/logout', [UserController::class, 'logout']);
                Route::post('/user', [UserController::class, 'getUser'])->middleware('verified');
            }
        );
    });

    //Verification Email Routes
    Route::get('/email/verify/{id}/{hash}', [VerifyEmailController::class, 'verify'])
        ->name('verification.verify');
    Route::get('email/resend', [VerifyEmailController::class, 'resend'])
        ->middleware('jwt.verify')
        ->name('verification.resend');
    Route::get('/email/notice', [VerifyEmailController::class, 'notice'])
        ->name('verification.notice');

    //Categories Routes
    Route::get('categories', [CategoryController::class, 'index']);
    Route::post('categories', [CategoryController::class, 'store']);
});
